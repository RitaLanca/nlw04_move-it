 # :rocket: Move-it

Move-it is an productivity application that applys the "Pomodoro Technique" with eye and body stretching  challenges between each Pomodoro.

![move-it application image] 


###  :tomato: Pomodoro Technique

Is a time management method that uses a timer to break down work into intervals, traditionally 25 minutes in length, separated by short breaks.


This project was developed during "NLW #04" an event promoted by [Rocketseat](https://rocketseat.com.br/) where were addressed the following contents:

### ReactJs:

* create-react-app
* SPA (Single Page Application)
* React components
* Context API

### NextJS

* create-next-app
* SSR (Server-side-rendering)
* SSG (Static Site Generation)
* Understanding SEO and Crawlers Indexing
* Intermediate layer in NodeJS


## Tecnologies

* NextJS
* ReactJS
* Typescript
* HTML
* CSS


## Pre-requisits

* Install Node + npm 
* Install Yarn


##  :wrench: Installing

First, clone the source code from repository:
```bash
git clone <repo_url>
```

Access the project folder:
```bash
cd move-it
```

Install the dependencies:
```bash
npm install
```

Run project at local machine:
```bash
npm run dev
# or
yarn dev
```

## License

This project is licensed under the MIT License - see the LICENSE.md file for details.