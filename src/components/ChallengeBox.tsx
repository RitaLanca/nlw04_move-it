import { useContext } from 'react';
import { ChallengeContext } from '../contexts/ChallengeContext';
import { CountDownContext } from '../contexts/CountDownContext';
import styles from '../styles/components/ChallengeBox.module.css'

export default function ChallengeBox() {
    const { activeChallenge, resetChallenge, completeChallenge } = useContext(ChallengeContext);
    const { resetCountDown } = useContext(CountDownContext);

    function handleChallengeSucceeded() {
        completeChallenge();
        resetCountDown();
    }

    function handleChallengeFailed() {
        resetChallenge();
        resetCountDown();
    }


    return (
        <div className={styles.challengeBoxContainer}>
            {activeChallenge ?
                (<div className={styles.challengeActive}>
                    <header>Ganhe {activeChallenge.amount}</header>
                    <main>
                        {activeChallenge.type === 'body'
                            ?
                            (<img src="/icons/body.svg" alt="exercise icon" />)
                            :
                            (<img src="/icons/eye.svg" alt="exercise icon" />)
                        }

                        <strong>Novo desafio</strong>
                        <p> {activeChallenge.description}s</p>
                    </main>
                    <footer>
                        <button type="button" className={styles.FailedBtn} onClick={handleChallengeFailed} >Falhei</button>
                        <button type="button" className={styles.SuccedeededBtn} onClick={handleChallengeSucceeded} >Completei</button>
                    </footer>
                </div>)
                :
                (<div className={styles.challengeNotActive}>
                    <strong>Finalize um ciclo para receber um desafio</strong>
                    <p>
                        <img src="icons/level-up.svg" alt="level Up" />
                        Avance de nivel completando desafios.
                    </p>
                </div>)

            }

        </div>
    )
}
