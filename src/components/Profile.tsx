import React, { useContext } from 'react'
import { ChallengeContext } from '../contexts/ChallengeContext'

import styles from '../styles/components/Profile.module.css'

export default function Profile() {

    const { level } = useContext(ChallengeContext);

    return (
        <div className={styles.profileContainer}>
            <img src="https://secure.gravatar.com/avatar/798f3d8bd4b0532f45b8f405bf108d53?s=180&d=identicon" alt="avatar" />
            <div>
                <strong>Rita</strong>
                <p>
                    <img src="/icons/level.svg" alt="Level" />
                    Level {level}
                </p>
            </div>
        </div>
    )
}
