import React, { useContext } from 'react'
import { ChallengeContext } from '../contexts/ChallengeContext'
import styles from '../styles/components/ExperienceBar.module.css'

export function ExperienceBar() {

    const { currentExperience, experienceToNextLevel } = useContext(ChallengeContext);

    //Math.round() para arrendondar as unidades
    const percentCompleted = Math.round(currentExperience * 100) / experienceToNextLevel;

    return (
        <header className={styles.experienceBar}>
            <span>0 xp</span>
            <div>
                <div style={{ width: `${percentCompleted}%` }}></div>
                <span className={styles.currentExperience} style={{ left: `${percentCompleted}%` }}>{currentExperience}xp</span>
            </div>
            <span>{experienceToNextLevel} xp</span>
        </header>
    )
}

