import { useContext } from 'react';
import { CountDownContext } from '../contexts/CountDownContext';
import Head from 'next/head';

import styles from '../styles/components/CountDown.module.css'


export default function CountDown() {
    const {
        timer,
        hasFinished,
        isActive,
        startCoundDown,
        resetCountDown
    } = useContext(CountDownContext);

    // const minutes = Math.floor(timer / 60);
    // const seconds = Math.floor(timer % 60);
    //Calcular minutos
    const minutes = Math.floor(timer / 60);
    //Calcular segundos
    const seconds = Math.floor(timer % 60);
    const [minuteLeft, minuteRight] = String(minutes).padStart(2, '0').split('');
    const [secondLeft, secondRight] = String(seconds).padStart(2, '0').split('');


    return (
        <div>
            {/* <Head>
                <title>{minuteLeft}{minuteRight}:{secondLeft}{secondRight}</title>
            </Head> */}
            <div className={styles.countDownContainer}>
                <div className={styles.timeWrapper}>
                    <span>{minuteLeft}</span>
                    <span>{minuteRight}</span>
                </div>
                <span>:</span>
                <div className={styles.timeWrapper}>
                    <span>{secondLeft}</span>
                    <span>{secondRight}</span>
                </div>
            </div>
            { hasFinished
                ?
                (<button
                    disabled
                    className={styles.countDownButton}>
                    Ciclo encerrado</button>)
                :
                (<>
                    {isActive ?
                        (<button
                            onClick={resetCountDown}
                            className={`${styles.countDownButton} ${styles.activeBtn}`}>
                            Abandonar ciclo</button>)
                        :
                        (<button
                            onClick={startCoundDown}
                            className={styles.countDownButton}>
                            Iniciar um ciclo</button>)}
                </>)
            }

        </div>
    )
}
