import { createContext, useState, ReactNode, useEffect } from 'react'
import Cookies from 'js-cookie'
import challenges from "../../challenges.json"
import LevelUpModal from '../components/LevelUpModal';

interface Challenge {
    type: 'body' | 'eye';
    description: string;
    amount: number;
}

interface ChallengeContextData {
    level: number;
    currentExperience: number;
    challengesCompleted: number;
    experienceToNextLevel: number;
    activeChallenge: Challenge;
    levelUp: () => void;
    startNewChallenge: () => void;
    resetChallenge: () => void;
    completeChallenge: () => void;
    closeLevelUpModal: () => void;

}

interface ChallengesProviderProps {
    children: ReactNode; /**aceita como filhos qualquer elemento -componente, texto, html, etc */
    level: number;
    currentExperience: number;
    challengesCompleted: number;
}

export const ChallengeContext = createContext({} as ChallengeContextData);

export function ChallengesProvider({ children, ...rest }: ChallengesProviderProps) {
    const [level, setLevel] = useState(rest.level ?? 1);
    const [currentExperience, setCurrentExperience] = useState(rest.currentExperience ?? 0);
    const [challengesCompleted, setChallengesCompleted] = useState(rest.challengesCompleted ?? 0);

    const [activeChallenge, setActiveChallenge] = useState(null);
    const [isLevelUpModalOpen, setIsLevelUpModalOpen] = useState(false);

    //rpg calcule experiemce to the next level 
    const factor = 4; //fator de experiencia que pode variar se quisermos que fique mais facil ou mais dificil
    const experienceToNextLevel = Math.pow((level + 1) * factor, 2);

    /**Permitir permissoes para mandar notificações */
    useEffect(() => {
        Notification.requestPermission();
    }, [])

    useEffect(() => {
        Cookies.set('level', String(level));
        Cookies.set('currentExperience', String(currentExperience));
        Cookies.set('challengesCompleted', String(challengesCompleted));

    }, [level, currentExperience, challengesCompleted])

    function levelUp() {
        setLevel(level + 1);
        setIsLevelUpModalOpen(true);
    }

    function closeLevelUpModal() {
        setIsLevelUpModalOpen(false);
    }

    function startNewChallenge() {
        const randomCHallengeIndex = Math.floor(Math.random() * challenges.length);
        const challenge = challenges[randomCHallengeIndex];
        setActiveChallenge(challenge);

        new Audio('/notification.mp3').play();

        if (Notification.permission === 'granted') {
            /**https://developer.mozilla.org/pt-PT/docs/Web/API/notification */
            new Notification('Novo desafio 🎉', {
                body: `Em jogo ${challenge.amount}xp!`
            });
        }
    }

    function resetChallenge() {
        setActiveChallenge(null);
    }

    function completeChallenge() {
        if (!activeChallenge) {
            return;
        }
        const { amount } = activeChallenge;

        let finalExperience = currentExperience + amount;
        console.log(currentExperience);

        if (finalExperience >= experienceToNextLevel) {
            //calcular o que sobra para o proximo nivel
            finalExperience = finalExperience - experienceToNextLevel;
            levelUp();//avançar de nivel
        }

        setCurrentExperience(finalExperience);
        console.log(currentExperience);
        setActiveChallenge(null); //remover o challenge que completamos como activo
        setChallengesCompleted(challengesCompleted + 1);
    }

    return (
        /**Todos os elementos dentro do provider vao ter acesso ao seu contexto */
        <ChallengeContext.Provider
            value={{
                level,
                currentExperience,
                challengesCompleted,
                levelUp,
                startNewChallenge,
                activeChallenge,
                resetChallenge,
                experienceToNextLevel,
                completeChallenge,
                closeLevelUpModal
            }}>
            {children}
            {isLevelUpModalOpen && <LevelUpModal />}

        </ChallengeContext.Provider>
    )
}