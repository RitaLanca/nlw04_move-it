import { createContext, ReactNode, useContext, useEffect, useState } from "react";
import { start } from "repl";
import { ChallengeContext } from "./ChallengeContext";

interface CounDownContextData {
    timer: number;
    hasFinished: boolean;
    isActive: boolean;
    resetCountDown: () => void;
    startCoundDown: () => void;
}

interface CountDownProviderProps {
    children: ReactNode; /**aceita como filhos qualquer elemento -componente, texto, html, etc */
}

export const CountDownContext = createContext({} as CounDownContextData);

/**Variavel global */
let countdownTimeout: NodeJS.Timeout;

export function CountDownProvider({ children }: CountDownProviderProps) {

    const { startNewChallenge } = useContext(ChallengeContext);
    const duration = 25 * 60;

    const [timer, setTimer] = useState(25 * 60); //25min * 60segundos
    const [isActive, setIsActive] = useState(false);
    const [hasFinished, setHasFinished] = useState(false);

    const [coundownTime, setCoundownTime] = useState(null);
    const [expectedTime, setExpectedTime] = useState(0);


    function startCoundDown() {
        setIsActive(true);
        //calcular em miliseconds qd foi iniciado a contagem e guardar numa variavel
        const getStartedCountingTime = new Date().getTime();
        //expectedTime é o tempo final quando termina a contagem 
        let expectedTime = getStartedCountingTime + (timer * 1000);
        setCoundownTime(expectedTime - getStartedCountingTime);
        setExpectedTime(expectedTime);
    }

    function resetCountDown() {
        /*cancela a execução do timeout, evitando o bug - "após fazer stop do countdown pára logo a contagem.
        Sem isto, após o reset ainda ocorre a diminuição de 1 segundo"*/
        clearTimeout(countdownTimeout);
        setCoundownTime(null);
        setIsActive(false);
        setTimer(25 * 60);
        setHasFinished(false);
    }

    function calculateLeftTime() {
        const now = new Date().getTime();
        // 1º calcular o nr de segundos que passou desde que o se iniciou o countdown
        // 2º retirar ao tempo inicial os nr de segundos que passaram
        const diff = expectedTime - new Date().getTime() > 0 ? expectedTime - new Date().getTime() : 0;

        setTimer(diff / 1000);

        return diff;
    }


    useEffect(() => {
        if (isActive && coundownTime - 1000 > 0) {
            setCoundownTime(calculateLeftTime());
            countdownTimeout = setTimeout(() => {
                setCoundownTime(calculateLeftTime());
            }, 1000);
        } else if (isActive) {
            setHasFinished(true);
            setIsActive(false);
            startNewChallenge();
            setExpectedTime(0);
        }
        return () => {
            clearTimeout(countdownTimeout);
        }
    });

    return (
        <CountDownContext.Provider
            value={{
                timer,
                hasFinished,
                isActive,
                resetCountDown,
                startCoundDown
            }}>
            {children}
        </CountDownContext.Provider>
    )
}