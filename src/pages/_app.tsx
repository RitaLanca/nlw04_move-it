import '../styles/global.css'

import { ChallengesProvider } from '../contexts/ChallengeContext'
import { useState } from 'react';
import { CountDownProvider } from '../contexts/CountDownContext';

//Componente que se repete em todas as páginas
function MyApp({ Component, pageProps }) {


  return (
    /**Todos os elementos dentro do provider vao ter acesso ao seu contexto */
    /**Como o CountDownContext usa o ChallengeContext, o CountDownProvider fica por dentro */
    // <ChallengesProvider>

    <Component {...pageProps} />

    // </ChallengesProvider>
  )
}

export default MyApp
