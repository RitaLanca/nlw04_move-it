import Document, { Html, Head, Main, NextScript } from 'next/document';


// O document apenas vai ser carregado uma unica vez, em toda a visita do user na app 
export default class MyCustomDocument extends Document {
    render() {
        return (
            <Html>
                <Head>
                    <link rel="shortcut icon" href="favicon.png" type="image/png" />
                    <link rel="preconnect" href="https://fonts.gstatic.com" />
                    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600&family=Rajdhani:wght@600&display=swap" rel="stylesheet" />
                </Head>
                <body>
                    {/* Onde vai mostrar a :app e as paginas */}
                    <Main />

                    {/* Scripts que o next injeta de forma automatizada*/}
                    <NextScript />
                </body>
            </Html>
        )
    }
}