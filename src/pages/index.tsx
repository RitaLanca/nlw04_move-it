import { GetServerSideProps } from 'next';
import Head from 'next/head'

import styles from '../styles/pages/Home.module.css'

import { ExperienceBar } from "../components/ExperienceBar";
import Profile from "../components/Profile";
import CompletedChallenges from '../components/CompletedChallenges';
import CountDown from '../components/CountDown';

import ChallengeBox from '../components/ChallengeBox';
import { CountDownProvider } from '../contexts/CountDownContext';
import React from 'react';
import { ChallengesProvider } from '../contexts/ChallengeContext';


interface HomeProps {
  level: number;
  currentExperience: number;
  challengesCompleted: number;
}

//Ponto de entrada da aplicação. Corresponde à homepage
export default function Home(props: HomeProps) {

  return (
    <ChallengesProvider
      level={props.level}
      currentExperience={props.currentExperience}
      challengesCompleted={props.challengesCompleted}>
      <div className={styles.container}>
        <Head>
          <title>Inicio | move.it</title>
        </Head>
        <ExperienceBar />

        {/*** Every Children component inside CountDownProvider have access to context variables */}
        <CountDownProvider>
          <section>
            <div>
              <Profile />
              <CompletedChallenges />
              <CountDown />
            </div>
            <div>
              <ChallengeBox />
            </div>
          </section>
        </CountDownProvider >

      </div>
    </ChallengesProvider>
  )
}


export const getServerSideProps: GetServerSideProps = async (ctx) => {

  //aceder ao request para ter acesso aos cookies
  const { level, currentExperience, challengesCompleted } = ctx.req.cookies;


  return {
    props: {
      level: Number(level),
      currentExperience: Number(currentExperience),
      challengesCompleted: Number(challengesCompleted)
    }
  }
}